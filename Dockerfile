#FROM ubuntu:22.04 as twingate-helper.sh-builder

#COPY twingate-helper.sh /
# hadolint ignore=DL3008,DL3015
#RUN apt-get -qq update && \
#    apt-get -yqq install gcc shc && \
#    rm -rf /var/lib/apt/lists/* && \
#    shc -f twingate-helper.sh && \
#    mv twingate-helper.sh.x twingate-helper

FROM ubuntu:22.04

ARG TWINGATE_CLIENT_VERSION
ARG IMAGE_VERSION
#ARG USER=twingate
#ARG UID=1010
#ARG GID=1010

LABEL org.opencontainers.image.description="Twingate Headless Client"
LABEL org.opencontainers.image.documentation="https://gitlab.com/papasy-foss/twingate-headless-client"
LABEL org.opencontainers.image.title="Twingate Headless Client"
#LABEL org.opencontainers.image.url="N/A"
#LABEL org.opencontainers.image.vendor="N/A"
LABEL org.opencontainers.image.version="$IMAGE_VERSION"
LABEL org.opencontainers.image.twingate.client.version="$TWINGATE_CLIENT_VERSION"


ENV SERVICE_KEY_PATH=/key.json \
    LOG_LEVEL=debug

WORKDIR /
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
# hadolint ignore=DL3008
RUN apt-get -qq update && \
    apt-get -yqq install --no-install-recommends ca-certificates curl dumb-init && \
    curl -k https://binaries.twingate.com/client/linux/install.sh | bash && \
    apt-get remove --purge --auto-remove -y && \
    rm -rf /var/lib/apt/lists/*


#    groupadd --gid "${GID}"  "${USER}" && \
#    adduser --disabled-password \    
#    --gecos "${USER} non root user" --home "/nonexistent" \    
#    --shell "/sbin/nologin" --no-create-home \    
#    --uid "${UID}" --gid "${GID}" "${USER}"
#COPY --from=twingate-builder /usr/bin/twingate /usr/bin/twingate
#COPY --from=twingate-builder /etc/twingate/ /etc/twingate/
#COPY --from=twingate-builder /usr/sbin/twingated /usr/sbin/twingated
#COPY --from=twingate-builder /usr/bin/dumb-init /usr/bin/dumb-init
#COPY --from=twingate-helper.sh-builder /twingate-helper /twingate-helper

COPY twingate-helper.sh /

ENTRYPOINT ["/usr/bin/dumb-init", "--"]

CMD ["/twingate-helper.sh" , "-s"]

HEALTHCHECK --interval=20s --timeout=10s \
    CMD /usr/bin/twingate status | grep Online || exit 1

#USER $USER