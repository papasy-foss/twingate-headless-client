#!/bin/bash
set -e

#trap graceful_shutdown SIGINT SIGQUIT SIGTERM

#function graceful_shutdown {
#  kill -SIGKILL 1
#  exit 0
#}

function usage {
    echo
    echo "   -s    Run pre flight checks and start twingate headless client"
    echo "   -h    Run healthchek on twingate"
    echo
}

function healthcheck {
    if ! command -v twingate
    then
        echo "twingate client is not installed "
        exit 1
    else
        /usr/bin/twingate status | grep Online || exit 1
    fi 
}

function pre_flight {
    echo
    echo "*** Running pre flight checks ***"
    echo 

    # check if SA key var has been set
    if [ -z "${SERVICE_KEY_PATH}" ]
    then
        echo "Error: missing env variable SERVICE_KEY_PATH"
        echo
        exit 1
    fi

    # check if log level env var has been set
    if [ -z "${LOG_LEVEL}" ]
    then
        echo "Error: missing env variable LOG_LEVEL"
        echo
        exit 1
    fi

    # check for SA key
    if [ ! -a "${SERVICE_KEY_PATH}" ] && [ ! -s "${SERVICE_KEY_PATH}" ]
    then
        echo "Error: Service key file doesn't exist or is an empty file"
        exit 1
    fi

    echo "*** Pre flight checks Passed***"
    echo
}

function start {

    echo "*** Running twingate setup ***"
    echo
    /usr/bin/twingate setup --log-level "${LOG_LEVEL}" --headless "${SERVICE_KEY_PATH}" || exit 1
    echo "*** Running twingate start ***"
    echo
    /usr/bin/twingate start
    exec sleep infinity
}

while getopts "sh" options
do
    case "${options}" in
        s) 
            pre_flight
            start
            ;;
        h) healthcheck ;;
        *) usage ; exit 1 ;;
    esac
done